import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import '/imports/routing/main';
import './main.html';
// import '/imports/global/vendor/tether/tether.js';
import '/imports/global/css/bootstrap.min.css';
import '/imports/global/css/bootstrap-extend.css';
import '/imports/assets/css/site.css';
import '/imports/assets/css/login.css';
import '/public/fonts/web-icons/web-icons.css';
// import '/imports/global/fonts/brand-icons/brand-icons.min.css';
import '/public/fonts/brand-icons/brand-icons.css';
// import '/imports/scss/site.scss';
// import '/imports/assets/css/site.css';
import '/imports/global/vendor/animsition/animsition.css';
// import '/imports/global/vendor/asscrollable/asScrollable.css';
// import '/imports/global/vendor/switchery/switchery.css';
// import '/imports/global/vendor/intro-js/introjs.css';
// import '/imports/global/vendor/slidepanel/slidePanel.css';
// import '/imports/global/vendor/flag-icon-css/flag-icon.css';
// import '/imports/global/vendor/breakpoints/breakpoints.js';

// Core
// import '/imports/global/vendor/babel-external-helpers/babel-external-helpers.js';
// import '/imports/global/vendor/jquery/jquery.js';
import '/imports/global/vendor/bootstrap/bootstrap.js';
// import '/imports/global/vendor/animsition/animsition.js';
// import '/imports/global/vendor/mousewheel/jquery.mousewheel.js';
// import '/imports/global/vendor/asscrollbar/jquery-asScrollbar.js';
// import '/imports/global/vendor/asscrollable/jquery-asScrollable.js';

// Plugins
// import '/imports/global/vendor/switchery/switchery.min.js';
import '/imports/global/vendor/intro-js/intro.js';
import '/imports/global/vendor/screenfull/screenfull.js';
import '/imports/global/vendor/slidepanel/jquery-slidePanel.js';
import '/imports/global/vendor/jquery-placeholder/jquery.placeholder.js';
// Scripts
import '/imports/lib/State.js';
import '/imports/lib/Component.js';
import { Plugin } from '/imports/lib/Plugin.js';
import '/imports/lib/Base.js';
import '/imports/lib/Config.js';
import '/imports/lib/Menubar.js';
import '/imports/lib/Sidebar.js';
import '/imports/lib/PageAside.js';
import '/imports/lib/menu.js';
// PageAside
import { Site } from '/imports/lib/Site.js';
import '/imports/lib/Plugin/asscrollable.js';
import '/imports/lib/Plugin/slidepanel.js';
import '/imports/lib/Plugin/switchery.js';
import '/imports/lib/Plugin/jquery-placeholder.js';

Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
  console.log("message", Site);
  
});

Template.hello.helpers({
  counter() {
    return Template.instance().counter.get();
  },
});

Template.hello.events({
  'click button'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
  },
});
