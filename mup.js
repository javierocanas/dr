module.exports = {
  servers: {
    one: {
      host: '45.55.141.97',
      username: 'root',
      pem: `${process.env.HOME}/.ssh/id_rsa`
      // password:
      // or leave blank for authenticate from ssh-agent
    }
  },

  meteor: {
    name: 'drquintanilla',
    path: process.env.PWD,
    servers: {
      one: {}
    },
    dockerImage: 'abernix/meteord:base',
    buildOptions: {
      serverOnly: true,
    },
    env: {
      ROOT_URL: 'http://45.55.141.97'
      // MONGO_URL: 'mongodb://localhost/meteor'
    },
    //dockerImage: 'kadirahq/meteord'
    deployCheckWaitTime: 60
  },
  mongo: {
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },
};