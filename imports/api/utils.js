import alertify from 'alertify.js';
import moment from 'moment';

export const formData = function (options) {
  const formValidator = Template.instance().$(options.selector);
  const data = {};
  let empty = 0;
  formValidator.find('input, select, textarea').each(function () {
    const element = $(this);
    const elementData = element.data();
    const elementValue = element.val();
    if (!elementValue) {
      element.addClass('error');
      if (empty == 0) {
        alertify.error("Algunos campos no han sido llenados");
        empty += 1;
      }
    } else {
      element.removeClass('error');
    }
    if (elementData.field) {
        data[elementData.field] = elementValue;
    }
  });
  if (empty == 0) {
    formValidator.get(0).reset();
  }
  return empty == 0 && data;
};

export const getMonthName = (index) => {
  const monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
  return monthNames[moment().get("month")];
}
