import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { formData } from '/imports/api/utils.js';
import Horarios from '/imports/collections/horarios';
import alertify from 'alertify.js';
import './horarios.html';

Template.horarios.events({ 
  "submit #horarios": function(event, template){ 
    event.preventDefault();
    const form = formData({
      selector: '#horarios'
    });
    if (this.data) {
      Horarios.update(this.data._id, { $set: form }, (e,r) => {
        if (!e) {
          $('#modal').modal('hide');
            alertify.success("Edicion exitosa");
        }
      });
    } else {
      if (form) {
        Horarios.insert(form, (e, r) => {
          if (!e) {
            alertify.success("Horario agregado con exito");
          }
        });
      }
    }
  } 
});