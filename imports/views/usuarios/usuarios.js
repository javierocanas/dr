import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { formData } from '/imports/api/utils.js';
import alertify from 'alertify.js';
import './usuarios.html';

Template.usuarios.events({ 
  "submit #usuarios": function(event, template){ 
    event.preventDefault();
    const form = formData({
      selector: '#usuarios'
    });
    const options = {
            email: form.email,
            password: form.password,
            username: form.email,
            profile: { 
              nombre: form.nombre,
              apellido: form.apellido,
              telefono: form.telefono,
              celular: form.celular,
              role: form.role
            }
          };
    if (this.data) {
      Meteor.users.remove(this.data._id, function(e, r){
        if (!e) {
          Meteor.call('PUT/ACCOUNTS/User', options, function(error, result){
            if(error){ 
              console.log("error", error); 
            } 
            if(result){ 
              console.log("inserted", result);
              $('#modal').modal('hide');
              alertify.success("Operacion exitosa");
            } 
          });
        }
      });
      return false;
    } else {
      Meteor.call('PUT/ACCOUNTS/User', options, function(error, result){
        if(error){ 
          console.log("error", error); 
        } 
        if(result){ 
          console.log("inserted", result);
          alertify.success("Operacion exitosa");
        } 
      });
    }
  } 
});