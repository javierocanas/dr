import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Site } from '/imports/lib/Site.js';
import { formData } from '/imports/api/utils.js';
import alertify from 'alertify.js';
import './login.html';

Template.login.events({ 
  "submit #login": function(event, template){
    event.preventDefault();
    const form = formData({
      selector: '#login'
    });
    Meteor.loginWithPassword(form.email, form.password, function (err) {
      if (!err) {
        FlowRouter.go('/clinicas');
        alertify.success("Hola de nuevo.");
      } else {
        alertify.error("Algo anda mal");
      }
    });
  } 
});