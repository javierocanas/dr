import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { formData } from '/imports/api/utils.js';
import Doctores from '/imports/collections/doctores';
import alertify from 'alertify.js';
import './doctores.html';

Template.doctores.events({ 
  "submit #doctores": function(event, template){ 
    event.preventDefault();
    const form = formData({
      selector: '#doctores'
    });
    if (this.data) {
      Doctores.update(this.data._id, { $set: form }, (e,r) => {
        if (!e) {
          $('#modal').modal('hide');
            alertify.success("Edicion exitosa");
        }
      });
    } else {
      if (form) {
        Doctores.insert(form, (e, r) => {
          if (!e) {
            alertify.success("Doctor agregado con exito");
          }
        });
      }
    }
  } 
});