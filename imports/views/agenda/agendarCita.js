import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { formData } from '/imports/api/utils.js';
import Citas from '/imports/collections/citas';
import alertify from 'alertify.js';
import './agendarCita.html';

Template.agendarCita.events({
  "submit #citas": function(event, template){
    event.preventDefault();
    const form = formData({
      selector: '#citas'
    });
    if (this.data) {
      Citas.update(this.data._id, { $set: form }, (e,r) => {
        if (!e) {
          $('#modal').modal('hide');
            alertify.success("Edicion exitosa");
        }
      });
    } else {
      if (form) {
        Citas.insert(form, (e, r) => {
          if (!e) {
            $('#modal').modal('hide');
            alertify.success("Cita agregada con exito");
          }
        });
      }
    }
  }
});
