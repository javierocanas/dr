import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { formData } from '/imports/api/utils.js';
import alertify from 'alertify.js';
import moment from 'moment';
import { ReactiveVar } from 'meteor/reactive-var';
import './agenda.css';
import './agenda.html';
import './agendarCita.js';
import {
  getMonthName
} from '/imports/api/utils.js';

Template.agenda.events({
  "click .fc-day": function(event, template){
    event.preventDefault();
    console.log("This is the data", this);
    Blaze.renderWithData(Template.modal, {
      id: 'modal',
      // header: 'Agendar Cita',
      modalContent: 'agendarCita',
      size: "modal-lg",
      // data: this,
    }, document.body);
  },
  "click .calendarView": (event, template) => {
    const viewMode = $(event.target).data('mode');
    $('#calendar').fullCalendar('changeView', viewMode);
  },
  "click #calender-prev": (event) => {
    $('#calendar').fullCalendar('prev');
  },
  "click #calender-next": (event) => {
    $('#calendar').fullCalendar('next');
  },
  "click #go-today": (event) => {
    $('#calendar').fullCalendar('today');
  },
  "click .calendarView, click .calendarActions": (event, template) => {
    const currentView = $('#calendar').fullCalendar('getView');
    template.title.set(currentView.title);
  },
});

Template.agenda.helpers({
  currentCalendarDate() {
    return Template.instance().title.get();
  },
  formatDate(date) {
    return moment(date).format('LLLL');
  },
  getStatus() {
    let status;
    if(this.active && !this.completed) {
      status = 'Activo';
    }else if( this.active && this.completed){
      status = 'Completado';
    }else {
      status = 'Pausado';
    }
    return status;
  },
  camelToNormal(str) {
    return str && str.replace(/([A-Z])/g, ' $1').replace(/^./, function(str){ return str.toUpperCase(); })
  },
  firstToUpper(str) {
    return str && str.charAt(0).toUpperCase() + str.slice(1);
  }
});

Template.agenda.onRendered(function() {
  const self = this;
  // const form = Forms.instance();
  $('#calendar').fullCalendar({
    lang: 'es',
    height: 500,
    header: false,
    editable: false,
    droppable: false,
    eventStartEditable: false,
    displayEventTime: false,
    hiddenDays: [0],
    events(start, end, start, callback) {
      const events = [];
      const query = self.filtro.get() && self.filtro.get() !== 'todas' ? {zona:self.filtro.get()} : {};
      // Proyectos.find(query).fetch().map((proyecto) => {
      //   if (!proyecto.completed) {
      //     events.push({
      //       title: `${proyecto.orden} - ${proyecto.cuadrilla}`,
      //       start: new Date(proyecto.start),
      //       end: new Date(proyecto.end),
      //       allDay: false,
      //       editable: true,
      //       id: proyecto._id,
      //       color: getProyectColor(proyecto)
      //     })
      //   }
      // });
      if (events.length > 0) {
        callback(events);
      }
    },
    eventRender(event, element) {
      element.find('#date-title').html(element.find('span.fc-event-title').text());
    },
    eventClick(event) {
      const query = Proyectos.findOne({
        _id: event.id
      });
      FlowRouter.go('proyectosView', {
        proyectoId: query._id
      })
    },

    eventDrop( event ) {
      const newDates = Object.assign({}, {
        start: event.start.format(),
        end: event.end.format()
      });
      updateProyecto({
        _id: event.id
      }, newDates);
    }
  });

  this.autorun( () => {
    // Proyectos.find().fetch();
    $('#calendar').fullCalendar('refetchEvents');
  });
});

Template.agenda.onCreated(function() {
  const currentDate = `${getMonthName()} ${moment().get("year")}`;
  this.title = new ReactiveVar(currentDate);
  this.proyectoToEdit = new ReactiveVar(null);
  this.filtro = new ReactiveVar(null);
  this.autorun(() => {
    this.subscribe('getProyectos');
  })
});
