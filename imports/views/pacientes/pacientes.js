import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { formData } from '/imports/api/utils.js';
import Pacientes from '/imports/collections/pacientes';
import alertify from 'alertify.js';
import './pacientes.html';

Template.pacientes.events({ 
  "submit #pacientes": function(event, template){ 
    event.preventDefault();
    const form = formData({
      selector: '#pacientes'
    });
    if (this.data) {
      Pacientes.update(this.data._id, { $set: form }, (e,r) => {
        if (!e) {
          $('#modal').modal('hide');
            alertify.success("Edicion exitosa");
        }
      });
    } else {
      if (form) {
        Pacientes.insert(form, (e, r) => {
          if (!e) {
            alertify.success("Paciente agregado con exito");
          }
        });
      }
    }
  } 
});