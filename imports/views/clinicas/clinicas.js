import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Site } from '/imports/lib/Site.js';
import { formData } from '/imports/api/utils.js';
import Clinicas from '/imports/collections/clinicas';
import alertify from 'alertify.js';
import './clinicas.html';

Template.clinicas.events({ 
  "submit #clinicas": function(event, template){ 
    event.preventDefault();
    const form = formData({
      selector: '#clinicas'
    });
    if (this.data) {
      Clinicas.update(this.data._id, { $set: form }, (e,r) => {
        if (!e) {
          $('#modal').modal('hide');
          alertify.success("Edicion exitosa");
        }
      });
    } else {
      if (form) {
        Clinicas.insert(form, (e, r) => {
          if (!e) {
            alertify.success("Clinica agregada con exito");
          }
        });
      }
    }
  },
});