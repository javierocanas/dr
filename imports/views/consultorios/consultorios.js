import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { formData } from '/imports/api/utils.js';
import Consultorios from '/imports/collections/consultorios';
import alertify from 'alertify.js';
import './consultorios.html';

Template.consultorios.events({ 
  "submit #consultorios": function(event, template){ 
    event.preventDefault();
    const form = formData({
      selector: '#consultorios'
    });
    if (this.data) {
      Consultorios.update(this.data._id, { $set: form }, (e,r) => {
        if (!e) {
          $('#modal').modal('hide');
            alertify.success("Edicion exitosa");
        }
      });
    } else {
      if (form) {
        Consultorios.insert(form, (e, r) => {
          if (!e) {
            alertify.success("Consultorio agregado con exito");
          }
        });
      }
    }
  } 
});