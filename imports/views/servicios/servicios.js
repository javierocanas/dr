import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { formData } from '/imports/api/utils.js';
import Servicios from '/imports/collections/servicios';
import alertify from 'alertify.js';
import './servicios.html';

Template.servicios.events({ 
  "submit #servicios": function(event, template){ 
    event.preventDefault();
    const form = formData({
      selector: '#servicios'
    });
    if (this.data) {
      Servicios.update(this.data._id, { $set: form }, (e,r) => {
        if (!e) {
          $('#modal').modal('hide');
            alertify.success("Edicion exitosa");
        }
      });
    } else {
      if (form) {
        Servicios.insert(form, (e, r) => {
          if (!e) {
            alertify.success("Servicio agregado con exito");
          }
        });
      }
    }
  } 
});