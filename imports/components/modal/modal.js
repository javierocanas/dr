import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import './modal.html';

Template.modal.onRendered(function () {
  $(`#${this.firstNode.id}`).modal('show');
});
Template.modal.events({
  "hidden.bs.modal": (event, templateInstance) => {
    Blaze.remove(templateInstance.view);
  }
});