import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import '/imports/dashboardLayout.js';
import '/imports/views/consultorios/consultorios.js';

FlowRouter.route('/consultorios', {
  name: 'consultorios',
  action: () => {
    BlazeLayout.render("dashboardLayout", {
      view: "consultorios"
    });
  }
});