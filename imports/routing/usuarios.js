import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import '/imports/dashboardLayout.js';
import '/imports/views/usuarios/usuarios.js';

FlowRouter.route('/usuarios', {
  name: 'usuarios',
  action: () => {
    BlazeLayout.render("dashboardLayout", {
      view: "usuarios"
    });
  }
});