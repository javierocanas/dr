import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import '/imports/plainLayout.js';
import '/imports/views/login/login.js';

FlowRouter.route('/login', {
  name: 'Login',
  action: () => {
    BlazeLayout.render("plainLayout", {
      view: "login"
    });
  }
});
