import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

FlowRouter.route('/', {
  triggersEnter: [function(context, redirect) {
    if (!Meteor.userId()) {
      redirect('/login');
    } else {
      redirect('/clinicas');
    }
  }],
  action: function(_params) {
    throw new Error("this should not get called");
  }
});