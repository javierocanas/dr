import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import '/imports/dashboardLayout.js';
import '/imports/views/doctores/doctores.js';

FlowRouter.route('/doctores', {
  name: 'doctores',
  action: () => {
    BlazeLayout.render("dashboardLayout", {
      view: "doctores"
    });
  }
});