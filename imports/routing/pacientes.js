import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import '/imports/dashboardLayout.js';
import '/imports/views/pacientes/pacientes.js';

FlowRouter.route('/pacientes', {
  name: 'pacientes',
  action: () => {
    BlazeLayout.render("dashboardLayout", {
      view: "pacientes"
    });
  }
});