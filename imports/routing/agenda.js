import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import '/imports/dashboardLayout.js';
import '/imports/views/agenda/agenda.js';

FlowRouter.route('/agenda', {
  name: 'agenda',
  action: () => {
    BlazeLayout.render("dashboardLayout", {
      view: "agenda"
    });
  }
});
