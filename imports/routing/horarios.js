import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import '/imports/dashboardLayout.js';
import '/imports/views/horarios/horarios';

FlowRouter.route('/horarios', {
  name: 'horarios',
  action: () => {
    BlazeLayout.render("dashboardLayout", {
      view: "horarios"
    });
  }
});