import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import '/imports/dashboardLayout.js';
import '/imports/views/clinicas/clinicas.js';

FlowRouter.route('/clinicas', {
  name: 'clinicas',
  action: () => {
    BlazeLayout.render("dashboardLayout", {
      view: "clinicas"
    });
  }
});