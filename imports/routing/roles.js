import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import '/imports/dashboardLayout.js';
import '/imports/views/roles/roles.js';

FlowRouter.route('/roles', {
  name: 'roles',
  action: () => {
    BlazeLayout.render("dashboardLayout", {
      view: "roles"
    });
  }
});