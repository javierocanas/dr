import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import Clinicas from '/imports/collections/clinicas';
import Consultorios from '/imports/collections/consultorios';
import Doctores from '/imports/collections/doctores';
import Horarios from '/imports/collections/horarios';
import Pacientes from '/imports/collections/pacientes';
import Servicios from '/imports/collections/servicios';
import alertify from 'alertify.js';

import './dashboardLayout.html';
import '/imports/components/modal/modal';

Template.dashboardLayout.events({ 
  "click #logout": function(event, template){ 
    Meteor.logout(function(err){ 
      if (!err) {
        FlowRouter.go('/');
      }
    });
  },
  "click #edit": function(event, template){
    event.preventDefault();
    console.log("This is the data", this);
    Blaze.renderWithData(Template.modal, {
      id: 'modal',
    //   header: this.name,
      modalContent: FlowRouter.getRouteName(),
      size: "modal-lg",
      data: this,
    }, document.body);
  }
});

Template.dashboardLayout.helpers({
  routeName: function(){
    function capitalize(s) {
      return s[0].toUpperCase() + s.slice(1)
    }
    return capitalize(FlowRouter.getRouteName());
  },
  registry: function() {
    if (FlowRouter.getRouteName() === 'clinicas') {
      return Clinicas.find().fetch();
    } else if (FlowRouter.getRouteName() === 'consultorios') {
      return Consultorios.find().fetch();
    } else if (FlowRouter.getRouteName() === 'doctores') {
      return Doctores.find().fetch();
    } else if (FlowRouter.getRouteName() === 'horarios') {
      return Horarios.find().fetch();
    } else if (FlowRouter.getRouteName() === 'pacientes') {
      return Pacientes.find().fetch();
    } else if (FlowRouter.getRouteName() === 'servicios') {
      return Servicios.find().fetch();
    } else if (FlowRouter.getRouteName() === 'usuarios') {
      let i;
      const users = Meteor.users.find().fetch();
      for(i = 0; i < users.length; i++){
          users[i].nombre = users[i]['username'];
          delete users[i].username;
      }
      return users;
    }
  }
});

Template.dashboardLayout.events({ 
  "click #remove": function(event, template){ 
    event.preventDefault();
    alertify.success('Registro borrado con exito');
    if (FlowRouter.getRouteName() === 'clinicas') {
      Clinicas.remove(this._id);
    } else if (FlowRouter.getRouteName() === 'consultorios') {
      Consultorios.remove(this._id);
    } else if (FlowRouter.getRouteName() === 'doctores') {
      Doctores.remove(this._id);
    } else if (FlowRouter.getRouteName() === 'horarios') {
      Horarios.remove(this._id);
    } else if (FlowRouter.getRouteName() === 'pacientes') {
      Pacientes.remove(this._id);
    } else if (FlowRouter.getRouteName() === 'servicios') {
      Servicios.remove(this._id);
    } else if (FlowRouter.getRouteName() === 'usuarios') {
      Meteor.users.remove(this._id);
    }
  } 
});

Template.registerHelper("selected", function (value, option) {
    return value === option ? 'selected' : '';
});