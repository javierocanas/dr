import { Meteor } from 'meteor/meteor';
import Clinicas from '/imports/collections/clinicas';
import Consultorios from '/imports/collections/consultorios';
import Doctores from '/imports/collections/doctores';
import Horarios from '/imports/collections/horarios';
import Pacientes from '/imports/collections/pacientes';
import Servicios from '/imports/collections/servicios';
import Citas from '/imports/collections/citas';

Meteor.startup(() => {
  // code to run on server at startup
  if (Meteor.users.find().count() === 0) {
    seedUserId = Accounts.createUser({
      email: 'admin@admin.com',
      password: 'admin',
      username: 'admin@admin.com'
    });
    console.log(`ADMIN USER CREATED ${seedUserId}`);
  }
});


Meteor.methods({
  'PUT/ACCOUNTS/User': (options) => {
    // check(options, Object);
    // options.username = options.email;
    let userId;
    if (options) {
      userId = Accounts.createUser(options);
      return userId;
    }
  }
});
